# JWS

## URL

`localhost:3000`

## Installation

```shell
docker-compose up
```

## Encode

1. Choose PKI(.pem)
example: choose ./keys/key.pem
2. Write passphrase(optional)
example: passphrase for ./keys/key.pem: `qweasd`
3. Write JSON
example:

```json
{
    "name":"name"
}
```

![encode](./content/image.png)
