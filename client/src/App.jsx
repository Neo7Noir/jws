import { useState } from 'react'
import axios from 'axios';
import './App.css'

function App() {
  const [error, setError] = useState('')
  const [jsonInput, setJsonInput] = useState('')
  const [privateKey, setPrivateKey] = useState(null)
  const [encode, setEncode] = useState('')
  const [passphrase, setPassphrase] = useState('')

  const fileChange = (e) => {
    setPrivateKey(e.target.files[0]);
  }

  const jsonInputChange = (e) => {
    setJsonInput(e.target.value);
  }

  const passphraseChange = (e) => {
    setPassphrase(e.target.value);
  }

  const submit = async () => {
    setEncode('')

    if(!jsonInput){
      setError("No json input")
      return
    }

    if(!privateKey){
      setError("No private key")
      return
    }

    const formData = new FormData();
    formData.append('privateKeyFile', privateKey);
    formData.append('jsonInput', jsonInput);
    formData.append('passphrase', passphrase);

    await axios.post('http://localhost:5000/encode', formData, {
      headers: { 'Content-Type': 'multipart/form-data' },
    }).then((response) => {
      console.log(response.data.signature)
      if(response.data.signature){
        setError('')
        setEncode(response.data.signature)
      }
      if(response.data.error){
        setEncode('')
        setError(response.data.error)
      }
    })
  }

  return (
    <div className='main-container'>
      <h1 className='title'>
        JWS
      </h1>
      

      <div className='container'>
        <input type='file' accept='.pem' onChange={fileChange}>
        </input>
        <div>
          <label >
            Passphrase:
            <input
              onChange={passphraseChange}
            />
          </label>
        </div>
      </div>


      <form className='json-text-area'>
          JSON:
          <textarea placeholder='Json text' onChange={jsonInputChange}/>
      </form>

      {
        error && 
        <div className='error-message-container'>
          {error}
        </div>
      }
      { 
        encode &&
        <div className='encode-message-container'>
          {encode}
        </div>
            
      }

      <button onClick={submit}>Submit</button>
    </div>
  )
}

export default App
